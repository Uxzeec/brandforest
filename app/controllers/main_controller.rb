class MainController < ApplicationController
  before_action :set_page_options

  def index
    @brands = Brand.limit(8)
    @hits = Product.all.limit (8)
  end

  def set_page_options
    @page_title = 'Brandforest'
    @page_description = 'Brandforest'
    @page_keywords = 'Brandforest'
  end
  end
