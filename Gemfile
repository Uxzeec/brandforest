#frozen_string_literal: true
source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

  ruby '2.7.2'
  gem 'meta-tags'
  gem 'breadcrumbs_on_rails'
  gem 'activestorage', '~> 5.2'
  gem 'bootstrap-sass', '~> 3.4.1'
  gem 'coffee-rails', '~> 4.2'
  gem 'faker' , git: 'https://github.com/faker-ruby/faker.git', branch: 'master'
  gem 'jbuilder'
  gem 'jquery-rails'
  gem 'oj'
  gem 'oj_mimic_json'
  gem 'pg'
  gem 'mysql2'
  gem 'puma', '~>3.11'
  gem 'rails','~>5.2.3'
  gem 'sassc-rails', '>= 2.1.0'
  gem 'turbolinks', '~> 5'
  gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'database_cleaner'
  gem 'factory_bot'
  gem 'pry'
  gem 'rails-controller-testing'
  gem 'rb-readline'
  gem 'rspec-json_expectations'
  gem 'rspec-rails'
end

group :development do
  gem 'listen'
  gem 'web-console', '>=3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'db-query-matchers'
  gem 'json_spec'
  gem 'launchy'
  gem 'rubocop',require:false
  gem 'shoulda-matchers'
  gem 'webdrivers'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
